<?php

namespace App\Http\Controllers;

use App\Models\main;
use Illuminate\Http\Request;

class mainController extends Controller
{
    public function __construct()
    {
        //$this->middleware('EnsureTokenIsValid');
    }
    
    //functions associated directly with endpoints
    public function show( Request $request )
    {
        $all_data = $this->returnAll();
        return response()->json( $all_data, 200 );
    }

    private function returnAll()
    {
        return main::all(); 
    }

    /**
     * Get all game ids by room id
     */
    public function getGameIds( Request $request )
    {
        $game_ids = $this->returnGameIdsByRoomId( $request->room_id );
        return response()->json( $game_ids, 200 );
    }

    /**
     * Get rooms ids by game id
     */
    public function getRooms( Request $request )
    {
        $room_id = $this->returnRoomIdByGameId( $request->game_id );
        return response()->json( $room_id, 200 );
    }
  
    /**
     * Get particular decision by game_id and room_id
     */
    public function getDecision( Request $request )
    {
        $decision = $this->returnDecisionByGameIdAndEvent( $request->game_id, $request->event );
        return response()->json( $decision, 200 );
    }

    /**
     * Get particular decision by game_id and room_id
     */
    public function getDecisionsByGameId( Request $request )
    {
        $decisions = $this->returnDecisionsByGameId( $request->game_id );
        return response()->json( $decisions, 200 );
    }

    /**
     * Get all the votes in the history by event
     */
    public function getGlobalDecision( Request $request )
    {
        $decisions = $this->returnAllDecisionsByEvent( $request->event );
        return response()->json( $decisions, 200 );
    }

    public function store( Request $request )
    {
        $main = main::create( $request->all() );
        return response()->json( [ "result" => "Success" ], 201 );
    }

    public function delete( Request $request )
    {
        $this->deleteRowsByGameId( $request->game_id, $request->room_id );
        return response()->json( [ "result" => "Success" ], 205 );
    }

    //
    //
    //Auxiliar functions that use database connection
    private function returnGameIdsByRoomId( $room_id )
    {
        $game_ids = main::where( 'room_id', $room_id )->get( 'game_id' );
        return $this->compressArray( $game_ids, 'game_id' );
    }

    private function returnRoomIdByGameId( $game_id )
    {
        $room_ids =  main::where( 'game_id', $game_id )->get( 'room_id' );
        return $this->compressArray( $room_ids, 'room_id' ); 
    }

    private function returnDecisionByGameIdAndEvent( $game_id, $event )
    {
        $result = main::where( 'game_id', $game_id )
        ->where( 'event' , $event )
        ->get( 'decision' );

        return $result[0];
    }

    private function returnDecisionsByGameId( $game_id )
    {
        $decisions = main::where( 'game_id', $game_id )->get( [ 'event', 'decision' ] );
        return $decisions;
    }

    private function returnAllDecisionsByEvent( $event )
    {
        $find_pro  = main::where( 'event', $event )->where( 'decision', "1" )->get( 'id' );
        $find_con  = main::where( 'event', $event )->where( 'decision', "0" )->get( 'id' );
        $count_pro = count( $find_pro );
        $count_con = count( $find_con );

        return 
            [
                'pro'  => $count_pro,
                'con'  => $count_con,
                'total'=> $count_con + $count_pro
            ];
    }

    private function deleteRowsByGameId( $game_id, $room_id )
    {
        $main = main::where( 'game_id', $game_id )
        ->where( 'room_id', $room_id );
        $main->delete();
    }
    //
    //
    //Other functions
    
    //Compress arrays with 
    public function compressArray( $array, $key )
    {
        $compressedArray = [];
        foreach( $array as $element ){
            $compressedArray[] = $element->$key;
        }
        $key_plural = sprintf( '%ss', $key );
        return [ $key_plural =>  $compressedArray ];
    }
    
}
