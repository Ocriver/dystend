## Simple app (laravel + postgres) to create differents endpoints to get, update or delete database info<br>
## POST request:<br>
-   domain/api/getAll<br>
    ```
    {}
    ```
- domain/api/getGameId - return game_ids with the same room<br>
    ``` 
    {
        "room_id" : "room_id"
    }
    ```
- domain/api/getRoom - return room_id from game_id<br>
    ```
    {
        "game_id" : "game_id"
    }
    ```
- domain/api/getDecision - return the event's decision made by a game_id <br>
    ``` 
    {
        "game_id" : "game_id",
        "event"   : "event",
    } 
    ```
- domain/api/getDecisions - return the all decision made by a game_id <br>
    ```
    {
        "game_id" : "game_id",
        "event"   : "event",
    }
    ```
- domain/api/getGlobalDecision - return the global count of an event<br>
    ```
    { 
        "event" : "event" 
    }
    ```
- domain/api/main/store - Insert new decision<br>
    ```
    {
        "game_id"   : "game_id",
        "room_id"   : "room_id",
        "event"     : "event",
        "decision"  : "decision" (true|false)
    }
    ```
## Delete request: <br>
- domain/api/rm/ - Remove all date related to a game_id<br>
    ```
     {
        "game_id"   : "game_id",
        "room_id"   : "room_id"
     }
    ```

# Security:<br>
The app need a **Bearer Token** in request for use it:<br>
- First, make a request to domain/api/register with:<br>
    ``` 
    {
       'name'     : 'name'
       'email'    : 'example@domain.com',
       'password' : 'password' (min lenght 8)
    }
    ```

- Then, with the user created, make a request to domain/api/login with:<br>
    ```
    {
       'email'    : 'example@domain.com',
       'password' : 'password'
    }
    ```
- In the response will be a token to use it as bearer token for the next request.<br>
