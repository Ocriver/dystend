<?php

namespace Database\Seeders;

use App\Models\main;
use Illuminate\Database\Seeder;

class MainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        main::truncate();

        $events = array(
            'Subida de impuesto A',
            'Subida de impuesto B',
            'Bajada de impuesto C'
        );
        $count_events = count($events);

        for( $j = 0; $j < 16; $j++ ){

            $game_id = $j;
            for( $i = 0; $i < $count_events; $i++ ){
                $room_id = $i;

                main::create([
                    'game_id'  => $game_id,
                    'room_id'  => $room_id,
                    'event'    => $events[$i],
                    'decision' => (bool) rand(0, 2),
                ]);
            }
        }
    
    }
}
