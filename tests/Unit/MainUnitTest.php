<?php

namespace Tests\Unit;

use App\Http\Controllers\mainController;
use PHPUnit\Framework\TestCase;

class MainUnitTest extends TestCase
{
    public function test_compressArray()
    {
        $room1 = new \stdClass();
        $room1->room_id = 1;
        $room2 = new \stdClass();
        $room2->room_id = 2;
        $room3 = new \stdClass();
        $room3->room_id = 3;

        $data = [
            $room1, $room2, $room3
        ];
        
        $mc          = new mainController();
        $result      = $mc->compressArray(  $data, 'room_id' );
        $good_result = [
            'room_ids' => [ 1, 2, 3 ]
        ];

        $this->assertEquals( $good_result, $result );
    }
}
