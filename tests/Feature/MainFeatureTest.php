<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MainTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return voidtests/Feature/MainTest.php
     */
    public function test_showStatus()
    {   
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post( '/api/main/', [] );

        $response->assertStatus(200);
    }

    public function test_getGameIdsStatus()
    {
        $data = [ "room_id" => 1 ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post('/api/getGameId/', $data);

        $response->assertStatus(200);
    }

    public function test_getRoomsStatus()
    {
        $data = [ "game_id" => 0 ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post('/api/getGameId/', $data);

        $response->assertStatus(200);
    }

    public function test_getDecisionStatus()
    {
        $data = [ "game_id" => 0, "event" => '%' ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post('/api/getGameId/', $data);

        $response->assertStatus(200);
    }

    public function test_getDecisionsByGameIdStatus()
    {
        $data = [ "game_id" => 0 ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post('/api/getGameId/', $data);

        $response->assertStatus(200);
    }

    public function test_globalGlobalDecisionStatus()
    {
        $data = [ "event" => '%' ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post('/api/getGameId/', $data);

        $response->assertStatus(200);
    }

    public function test_storeStatus()
    {
        $data = [ 
            "game_id" => "dummy",
            "room_id" => "dummy",
            "event"   => "dummy",
            "decision"=> "false"
         ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post('/api/main/store/', $data );

        $response->assertStatus(201);
    }

    /**
     * @depends test_storeStatus
     */
    public function test_deleteStatus()
    {
        $data = [ 
            "game_id" => "dummy",
            "room_id" => "dummy"
         ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->delete('/api/rm/', $data );

        $response->assertStatus(205);
    }

    public function test_show()
    {   
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post( '/api/main/', [] );

        $this->assertIsArray( $response->json() );
    }

    public function test_getGameIds()
    {
        $data = [ "room_id" => 1 ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post('/api/getGameId/', $data);

        $this->assertIsArray( $response->json() );
        $this->assertArrayHasKey( 'game_ids', $response->json() );
    }

    public function test_getRooms()
    {
        $data = [ "game_id" => 0 ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post('/api/getRoom/', $data);

        $this->assertIsArray( $response->json() );
        $this->assertArrayHasKey( 'room_ids', $response->json() );
    }

    public function test_getRoomsEmptyValue()
    {
        $data = [ "game_id" => null ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post( '/api/getRoom/', $data );

        $this->assertIsArray( $response->json() );
        $this->assertArrayHasKey( 'room_ids', $response->json() );
        $this->assertEmpty( $response->json()['room_ids'] );
    }

    public function test_store()
    {
        $data = [ 
            "game_id" => "dummy",
            "room_id" => "dummy",
            "event"   => "dummy",
            "decision"=> "0"
         ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post('/api/main/store/', $data );
        $good_response = [ "result" => "Success" ];
        $this->assertEquals( $good_response, $response->json() );
    }

    /*TODO: need ids and events dummy or mock database functions*/

    public function test_getDecision()
    {
        $data = [ "game_id" => "dummy", "event" => "dummy" ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post('/api/getDecision/', $data);

        $this->assertIsArray( $response->json() );
        $this->assertArrayHasKey( 'decision', $response->json() );
    }

    public function test_getDecisionsByGameId()
    {
        $data = [ "game_id" => 0 ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post('/api/getDecisions/', $data);

        $this->assertIsArray( $response->json() );
        $this->assertIsArray( current( $response->json() ) );
        $this->assertArrayHasKey( 'event', current( $response->json() ) );
        $this->assertArrayHasKey( 'decision', current( $response->json() ) );
    }

    public function test_getGlobalDecision()
    {
        $data = [ "event" => 'dummy' ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->post( '/api/getGlobalDecision/', $data );

        $this->assertIsArray( $response->json() );
        
    }

    /**
     * @depends test_store
     */
    public function test_delete()
    {   
        $data = [ 
            "game_id" => "dummy-feature",
            "room_id" => "dummy-feature"
         ];
        $user = User::factory()->create();
        $response = $this->actingAs( $user )
                         ->withSession( [ 'banned' => false ] )
                         ->delete('/api/rm', $data );
        $good_response = [ "result" => "Success" ];
        $this->assertEquals( $good_response, $response->json() );
    }

}
