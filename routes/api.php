<?php
use App\Http\Controllers\mainController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Get data
Route::post( 'main',              [ mainController::class, 'show'] )->middleware('auth:sanctum');
Route::post( 'getGameId',         [ mainController::class, 'getGameIds'] )->middleware('auth:sanctum');
Route::post( 'getRoom',           [ mainController::class, 'getRooms'] )->middleware('auth:sanctum');
Route::post( 'getDecision',       [ mainController::class, 'getDecision'] )->middleware('auth:sanctum');
Route::post( 'getDecisions',      [ mainController::class, 'getDecisionsByGameId'] )->middleware('auth:sanctum');
Route::post( 'getGlobalDecision', [ mainController::class, 'getGlobalDecision'] )->middleware('auth:sanctum');

//Insert
Route::post( 'main/store', [ mainController::class, 'store'] )->middleware('auth:sanctum');

//Delete 
Route::delete( 'rm' , [ mainController::class, 'delete'] )->middleware('auth:sanctum');

//Auth
Route::post( '/register' , [AuthController::class, 'register']);

Route::post( '/login'    , [AuthController::class, 'login']);